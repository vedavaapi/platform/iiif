from setuptools import setup

try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except:
    long_description = ''

setup(
    name='iiif',
    version='1.0.0',
    packages=['vedavaapi', 'vedavaapi.iiif', 'vedavaapi.iiif.iiif_image', 'vedavaapi.iiif.iiif_presentation'],
    url='https://github.com/vedavaapi',
    author='vedavaapi',
    description='iiif api implementation',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=['loris', 'six', 'iiif-prezi', 'flask'],
    classifiers=(
            "Programming Language :: Python :: 2.7",
            "Operating System :: OS Independent",
    )
)
