
from vedavaapi.objectdb.helpers import projection_helper, ObjModelException


class IIIFModelInterface(object):

    service_name = None

    def get_collection_model(self, collection_id):
        pass

    def get_object_model(self, object_id):
        pass

    def get_sequence_model(self, object_id, sequence_id):
        pass

    def get_canvas_model(self, object_id, canvas_id):
        pass


class VedavaapiIIIFModelInterface(IIIFModelInterface):

    service_name = None

    def __init__(self, org_name, colln, vv_schemas):
        self.org_name = org_name
        self.colln = colln
        self.vv_schemas = vv_schemas

    def get_model_definition(self, resource):
        json_class = resource['jsonClass']
        vv_schema = self.vv_schemas.get(json_class)

        if not vv_schema or '_iiif_model' not in vv_schema:
            return None

        return vv_schema['_iiif_model'].copy()

    @classmethod
    def get_data(cls, resource_json, data_fields_config):
        data = {}
        for k in data_fields_config.keys():
            field_name = data_fields_config[k]
            try:
                val = projection_helper.get_matched_nested_value(resource_json, field_name)
            except ObjModelException:
                continue
            if val is not None:
                data[k] = val
        return data

    @classmethod
    def get_linked_ids(cls, colln, resource_json, links_config):
        link_ids = []
        links_through_fields = links_config.get('fields', None)
        if links_through_fields:
            for field in links_through_fields:
                try:
                    val = projection_helper.get_matched_nested_value(resource_json, field)
                except ObjModelException:
                    continue
                if val is None:
                    continue
                if isinstance(val, list):
                    link_ids.extend(val)
                else:
                    link_ids.append(val)
            return link_ids

        else:
            referrer_field_filter_maps = links_config.get('referrer_field_filter_maps', {})
            for field, filter_doc in referrer_field_filter_maps.items():
                filter_doc = filter_doc.copy()
                filter_doc.update({
                    field: resource_json['_id']
                })
                matched_docs = colln.find(filter_doc, projection={"_id": 1})
                matched_docs_ids = [doc['_id'] for doc in matched_docs]
                link_ids.extend(matched_docs_ids)
            return link_ids

    def get_collection_model(self, collection_id):
        resource_json = self.colln.find_one({"_id": collection_id}, projection={"permissions": 0})
        if not resource_json:
            return None
        model_definition = self.get_model_definition(resource_json)
        if not model_definition or 'collection' not in model_definition['implements']:
            return None

        data_fields_config = model_definition['data_fields']
        data =self.get_data(resource_json, data_fields_config)

        sub_collection_links_config = model_definition.get('sub_collection_links', {})
        sub_collection_ids = self.get_linked_ids(
            self.colln, resource_json, sub_collection_links_config)

        object_links_config = model_definition.get('object_links', {})
        object_ids = self.get_linked_ids(
            self.colln, resource_json, object_links_config)

        return {
            "data": data,
            "sub_collection_ids": sub_collection_ids,
            "object_ids": object_ids
        }

    def get_object_model(self, object_id):
        resource_json = self.colln.find_one({"_id": object_id}, projection={"permissions": 0})
        if not resource_json:
            return None
        model_definition = self.get_model_definition(resource_json)
        if not model_definition or 'object' not in model_definition['implements']:
            return None

        data_fields_config = model_definition['data_fields']
        data = self.get_data(resource_json, data_fields_config)

        sequence_links_config = model_definition.get('sequence_links', {})
        sequence_ids = self.get_linked_ids(self.colln, resource_json, sequence_links_config)
        print({"slc": sequence_links_config, "si": sequence_ids})

        default_sequence_links_config = model_definition.get('default_sequence_links', {})
        default_sequence_ids = self.get_linked_ids(
            self.colln, resource_json, default_sequence_links_config)

        if not len(default_sequence_ids):
            if model_definition.get('fallback_on_self_for_default_sequence', None):
                default_sequence_ids.append(object_id)
            elif len(sequence_ids):
                default_sequence_ids.append(sequence_ids[0])
        non_default_sequence_ids = list(set(sequence_ids).difference(default_sequence_ids))

        return {
            "data": data,
            "sequence_ids": non_default_sequence_ids,
            "default_sequence_ids": default_sequence_ids
        }

    def get_sequence_model(self, object_id, sequence_id):
        resource_json = self.colln.find_one({"_id": sequence_id}, projection={"permissions": 0})
        if not resource_json:
            return None
        model_definition = self.get_model_definition(resource_json)
        if not model_definition or 'sequence' not in model_definition['implements']:
            return None

        data_fields_config = model_definition['data_fields']
        data = self.get_data(resource_json, data_fields_config)

        model = {
            "data": data
        }

        canvas_links_config = model_definition.get('canvas_links', {})
        canvas_infos = self.get_linked_ids(self.colln, resource_json, canvas_links_config)

        if len(canvas_infos) and isinstance(canvas_infos[0], dict):
            model['canvas_descriptors'] = canvas_infos
            if 'index_props_field' in model_definition:
                index_props_field = model_definition['index_props_field']
                index_props = resource_json.get(index_props_field, None)
                if index_props:
                    model['index_props'] = index_props
        else:
            model['canvas_ids'] = canvas_infos

        return model

    def get_canvas_model(self, object_id, canvas_id):
        resource_json = self.colln.find_one({"_id": canvas_id}, projection={"permissions": 0})
        if not resource_json:
            return None
        model_definition = self.get_model_definition(resource_json)
        if not model_definition or 'canvas' not in model_definition['implements']:
            return None

        data_fields_config = model_definition['data_fields']
        data = self.get_data(resource_json, data_fields_config)
        '''
        if 'label' not in data:
            if 'label_prefix' not in data:
                data['label_prefix'] = 'page: '
        '''
        data['label'] = data.get('label', data.get('label_prefix', '_'))

        image_links_config = model_definition.get('image_links', {})
        image_ids = self.get_linked_ids(self.colln, resource_json, image_links_config)
        image_ids = [image_id[6:] if image_id.startswith('_OOLD:') else image_id for image_id in image_ids]

        if not len(image_ids):
            return None
        return {
            "data": data,
            "image_ids": image_ids
        }
