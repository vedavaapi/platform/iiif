import re
from collections import namedtuple
from functools import reduce

from vedavaapi.iiif_image.loris.resolver import FSInterface

try:
    # noinspection PyCompatibility
    from urllib.parse import unquote, quote_plus, urljoin, urlparse
except ImportError:  # Python 2
    # noinspection PyUnresolvedReferences
    from urllib import unquote, quote_plus
    # noinspection PyUnresolvedReferences,PyCompatibility
    from urlparse import urljoin, urlparse

from iiif_prezi.factory import *

from .resource_factory import CustomManifestFactory
from .iiif_model_helper import VedavaapiIIIFModelInterface


# noinspection PyMethodMayBeStatic,PyProtectedMember
class BaseMetadataObjectEditor(object):
    essential_meta_props = []
    essential_props = []
    resource_cls = BaseMetadataObject

    def __init__(self, editor_factory, uri_segment):
        """
        initializes all Editor Objects.
        takes editor_factory as param, so that every resource_editor can call editors of other resources.
        :param editor_factory:
        """
        self.editor_factory = editor_factory  # type: PresentationEditorFactory
        self.uri_segment = uri_segment
        self.service_iiif_model_interface = self.editor_factory.vv_service_iiif_model_interface  # type: VedavaapiIIIFModelInterface
        self.fs_interface = self.editor_factory.fs_interface  # type: FSInterface
        self.resource_factory = self.editor_factory.resource_factory  # type: ManifestFactory

    def _new_base(self, *args, **kwargs):
        pass

    def full_uri_segment(self, *args, **kwargs):
        pass

    @classmethod
    def update_props(cls, resource, data):
        for prop, val in data.items():
            if prop == 'metadata':
                val = dict((mi['label'], mi['value']) for mi in val)
            setattr(resource, prop, val)
        return resource


class CollectionEditor(BaseMetadataObjectEditor):
    essential_meta_props = ['label']
    essential_props = ['label']
    resource_cls = Manifest

    def full_uri_segment(self, collection_id):
        return '{}/{}'.format(
            self.uri_segment,
            collection_id
        )

    def _new_base(self, collection_id, data, container=None):
        data = data.copy()
        for prop in self.__class__.essential_meta_props:
            if prop not in data:
                return None
        ident = self.full_uri_segment(collection_id)
        collection = (container or self.resource_factory).collection(ident=ident, label=data.pop('label'))
        self.update_props(collection, data)
        return collection

    def register_manifests(self, collection, object_ids):
        for object_id in object_ids:
            object_model = self.service_iiif_model_interface.get_object_model(object_id)
            if object_model is None:
                continue
            # noinspection PyProtectedMember
            self.editor_factory.manifest_editor._new_base(
                object_id, object_model.get('data', {}), container=collection
            )

    def register_collections(self, collection, sub_collection_ids):
        for sub_collection_id in sub_collection_ids:
            sub_collection_model = self.service_iiif_model_interface.get_collection_model(sub_collection_id)
            if sub_collection_model is None:
                continue
            self._new_base(
                sub_collection_id, sub_collection_model.get('data', {}), container=collection)

    def from_details(self, collection_id, data, object_ids, sub_collection_ids=None):
        collection = self._new_base(collection_id, data)
        if object_ids is not None:
            self.register_manifests(collection, object_ids)
        if sub_collection_ids is not None:
            self.register_collections(collection, sub_collection_ids)
        return collection

    def create(self, collection_id):
        collection_model = self.service_iiif_model_interface.get_collection_model(collection_id)
        if collection_model is None:
            return None

        return self.from_details(
            collection_id, collection_model.get('data', None),
            collection_model.get('object_ids', []),
            sub_collection_ids=collection_model.get('sub_collection_ids', [])
        )


# noinspection PyMethodMayBeStatic
class ManifestEditor(BaseMetadataObjectEditor):
    essential_meta_props = ['label']
    essential_props = ['label']
    resource_cls = Manifest

    def full_uri_segment(self, object_id):
        return '{}/{}'.format(
            object_id,
            self.uri_segment
        )

    def _new_base(self, object_id, data, container=None):
        data = data.copy()
        for prop in self.__class__.essential_meta_props:
            if prop not in data:
                return None
        ident = self.full_uri_segment(object_id)
        manifest = (container or self.resource_factory).manifest(ident=ident, label=data.pop('label'))
        self.update_props(manifest, data)
        return manifest

    def add_sequence(self, manifest, sequence):
        manifest.add_sequence(sequence)

    # noinspection PyUnusedLocal
    def from_details(self, object_id, data, default_sequence_ids, sequence_ids=None):
        manifest = self._new_base(object_id, data)
        default_sequence = self.editor_factory.sequence_editor.create(object_id, default_sequence_ids[0])
        if not manifest or not default_sequence:
            return None
        self.add_sequence(manifest, default_sequence)
        if sequence_ids and len(sequence_ids):
            for sequence_id in sequence_ids:
                sequence = self.editor_factory.sequence_editor._new_base(object_id, sequence_id, {})
                manifest.sequences.append(sequence.id)
        return manifest

    def create(self, object_id):
        object_model = self.service_iiif_model_interface.get_object_model(object_id)
        if object_model is None:
            return None
        return self.from_details(
            object_id,
            object_model.get('data', {}),
            object_model.get('default_sequence_ids', []),
            object_model.get('sequence_ids', []))


# noinspection PyMethodMayBeStatic
class SequenceEditor(BaseMetadataObjectEditor):
    essential_meta_props = []
    essential_props = []
    resource_cls = Sequence

    def full_uri_segment(self, object_id, sequence_id):
        return '{}/{}/{}'.format(
            object_id,
            self.uri_segment,
            sequence_id
        )

    def _new_base(self, object_id, sequence_id, data, container=None):
        data = data.copy()
        for prop in self.__class__.essential_meta_props:
            if prop not in data:
                return None
        ident = self.full_uri_segment(object_id, sequence_id)
        sequence = (container or self.resource_factory).sequence(ident=ident)
        self.update_props(sequence, data)
        return sequence

    def add_canvas(self, sequence, canvas):
        sequence.add_canvas(canvas)

    def add_canvases(self, sequence, object_id, canvas_ids):
        for canvas_id in canvas_ids:
            canvas = self.editor_factory.canvas_editor.create(object_id, canvas_id)
            if canvas is None:
                continue
            self.add_canvas(sequence, canvas)

    def add_canvases_from_descriptors(self, sequence, object_id, canvas_descriptors):
        for i, canvas_descriptor in enumerate(canvas_descriptors):
            canvas_id = canvas_descriptor['resource']
            canvas = self.editor_factory.canvas_editor.create(object_id, canvas_id)
            if canvas is None:
                continue
            if canvas.label == '_':
                canvas.label = canvas_descriptor.get('index', str(i))
            self.add_canvas(sequence, canvas)

    def sort_canvas_descriptors(self, canvas_descriptors, index_props=None):
        if not index_props:
            return canvas_descriptors[:]
        index_parts_refex = re.compile(index_props['indexPartsRegex'])
        index_parts_max_lengths = index_props.get('indexPartsMaxLengths', [])
        pad_char = index_props.get('padChar', str(0))

        def sort_key_fn(canvas_descriptor):
            index = canvas_descriptor['index']
            index_parts_match = index_parts_refex.match(index)
            index_parts = index_parts_match.groups()
            uniform_index_parts = []

            for i, index_part in enumerate(index_parts):
                if i < len(index_parts_max_lengths):
                    max_length = index_parts_max_lengths[i]
                elif len(index_parts_max_lengths) > 0:
                    max_length = index_parts_max_lengths[-1]
                else:
                    max_length = 7

                uniform_index_part = str(pad_char * (max_length - len(index_part))) + index_part
                uniform_index_parts.append(uniform_index_part)

            return '.'.join(uniform_index_parts)

        return sorted(canvas_descriptors, key=sort_key_fn)

    def from_details(
            self, object_id, sequence_id, data, canvas_ids=None, canvas_descriptors=None, index_props=None):
        sequence = self._new_base(object_id, sequence_id, data)
        if canvas_ids:
            self.add_canvases(sequence, object_id, canvas_ids)
        elif canvas_descriptors:
            self.sort_canvas_descriptors(canvas_descriptors, index_props)
            self.add_canvases_from_descriptors(sequence, object_id, canvas_descriptors)
        return sequence

    def create(self, object_id, sequence_id):
        sequence_model = self.service_iiif_model_interface.get_sequence_model(object_id, sequence_id)
        if sequence_model is None:
            return None
        return self.from_details(
            object_id, sequence_id, sequence_model.get('data', {}),
            canvas_ids=sequence_model.get('canvas_ids', None),
            canvas_descriptors=sequence_model.get('canvas_descriptors', None),
            index_props=sequence_model.get('index_props')
        )


class CanvasEditor(BaseMetadataObjectEditor):
    essential_meta_props = ['label']
    essential_props = ['label', 'height', 'width']
    resource_cls = Canvas

    def full_uri_segment(self, object_id, canvas_id):
        return '{}/{}/{}'.format(
            object_id,
            self.uri_segment,
            canvas_id
        )

    def _new_base(self, object_id, canvas_id, data, container=None):
        data = data.copy()
        for prop in self.__class__.essential_meta_props:
            if prop not in data:
                return None
        ident = self.full_uri_segment(object_id, canvas_id)
        canvas = (container or self.resource_factory).canvas(
            ident=ident, label=str(data.pop('label')), mdhash=data.pop('mdhash', {})
        )
        self.update_props(canvas, data)
        return canvas

    def set_full_image_annotation(self, canvas, image_oold):
        iiif_image_ident = '{}/{}'.format(
            self.service_iiif_model_interface.service_name,
            image_oold['_id']
        )
        dimensions = image_oold.get('namespaceData', {}).get('dimensions')
        if dimensions:
            return self.create_image_annotation_with_dimensions(
                canvas, iiif_image_ident, dimensions)
        return self.create_image_annotation(canvas, iiif_image_ident)
        # print('set_full_image_called', canvas, image_id)
        # return canvas.set_image_annotation(iiif_image_ident, iiif=True)

    def set_external_image_annotation(self, canvas, image_oold):
        iiif_image_resource = self.fs_interface.get_iiif_image_resource(image_oold)
        if iiif_image_resource:
            iiif_image_resource.pop('type', None)
            iiif_service_json = iiif_image_resource.pop('service')
            # noinspection PyProtectedMember
            iiif_service = ImageService(
                canvas._factory, iiif_service_json.pop('@id'), context=iiif_service_json.pop('@context'),
                profile=iiif_service_json.pop('profile'))
            anno = canvas.annotation()
            image = anno.image(ident=iiif_image_resource.pop('@id'))
            setattr(image, 'service', iiif_service)
            self.update_props(image, iiif_image_resource)
            return anno

    def create_image_annotation_with_dimensions(
            self, canvas, iiif_image_ident, dimensions):
        anno = canvas.annotation()
        img = anno.image(iiif_image_ident, iiif=True)
        img.width = dimensions['width']
        img.height = dimensions['height']
        canvas.height = img.height
        canvas.width = img.width
        return anno

    # deprecated
    def create_image_annotation(self, canvas, iiif_image_ident):
        full_url = self.resource_factory.default_base_image_uri.rstrip('/') + '/' + iiif_image_ident + '/info.json'
        path = urlparse(full_url).path
        request_cls = namedtuple('Request', ["url", "path"])
        request = request_cls(full_url, path)

        from .. import VedavaapiIiifPresentation
        service = VedavaapiIiifPresentation.instance
        loris_wrapper = service.registry.lookup('iiif_image').loris_wrapper(self.service_iiif_model_interface.org_name)
        # noinspection PyProtectedMember
        info = loris_wrapper.loris._get_info(
            iiif_image_ident, request, self.resource_factory.default_base_image_uri+'/'+iiif_image_ident)[0]

        return self.create_image_annotation_with_dimensions(
            canvas, iiif_image_ident, {"width": info.width, "height": info.height}
        )

    # noinspection PyUnusedLocal
    def from_details(self, object_id, canvas_id, data, image_id=None, image_ids=None, dimensions=None):
        canvas = self._new_base(object_id, canvas_id, data)
        if image_id is not None:
            image_oold = self.fs_interface.get_ool_data(image_id)
            if not image_oold:
                return canvas
            if self.fs_interface.provides_external_iiif(image_oold):
                self.set_external_image_annotation(canvas, image_oold)
            else:
                self.set_full_image_annotation(canvas, image_oold)
            setattr(canvas.images[0].resource, 'oold_id', image_id)
        return canvas

    def create(self, object_id, canvas_id):
        canvas_model = self.service_iiif_model_interface.get_canvas_model(object_id, canvas_id)
        if canvas_model is None:
            return None
        return self.from_details(
            object_id, canvas_id, canvas_model.get('data', {}), image_id=canvas_model['image_ids'][0]
        )


class LayerEditor(BaseMetadataObjectEditor):
    essential_meta_props = ['label']
    essential_props = ['label']
    resource_cls = Layer

    def full_uri_segment(self, object_id, layer_id):
        return '{}/{}/{}'.format(
            object_id,
            self.uri_segment,
            layer_id
        )

    # TODO


EditorDefinition = namedtuple('EditorDefinition', ('editor_class', 'uri_segment'))


# noinspection PyMethodMayBeStatic
class PresentationEditorFactory(object):
    editor_definitions = {
        'collection': EditorDefinition(CollectionEditor, 'collection'),
        'manifest': EditorDefinition(ManifestEditor, 'manifest'),
        'sequence': EditorDefinition(SequenceEditor, 'sequence'),
        'canvas': EditorDefinition(CanvasEditor, 'canvas'),
        'layer': EditorDefinition(LayerEditor, 'layer')
    }

    def __init__(self, vv_service_iiif_model_interface, fs_interface, resource_factory):
        self.vv_service_iiif_model_interface = vv_service_iiif_model_interface
        self.fs_interface = fs_interface
        self.resource_factory = resource_factory
        self._ed_instance_registry = {}

    @classmethod
    def from_details(
            cls, vv_service_iiif_model_interface, fs_interface, image_api_client,
            base_prezi_uri, base_prezi_dir,
            base_image_uri, iiif_image_info=(2.0, 2)):

        resource_factory = CustomManifestFactory(image_api_client)
        resource_factory.set_debug('error')

        full_base_prezi_uri = reduce(lambda base, segm: urljoin(base.rstrip('/') + '/', segm.lstrip('/')), [
            base_prezi_uri,
            vv_service_iiif_model_interface.service_name,
        ])
        #  print({"base_prezi_uri": (base_image_uri, full_base_prezi_uri)})
        resource_factory.set_base_prezi_uri(full_base_prezi_uri)

        resource_factory.set_base_image_uri(base_image_uri)
        resource_factory.set_iiif_image_info(*iiif_image_info)
        return cls(vv_service_iiif_model_interface, fs_interface, resource_factory)

    def resource_uri(self, uri_segment):
        return urljoin(
            self.resource_factory.prezi_base,
            uri_segment.lstrip('/') + '.json'
        )

    def _editor_instance(self, resource_type, force=False):
        if resource_type not in self.editor_definitions:
            return None
        instance = self._ed_instance_registry.get(resource_type, None)
        if (instance is None) or force:
            editor_definition = self.editor_definitions[resource_type]
            self._ed_instance_registry[resource_type] = editor_definition.editor_class(
                self, editor_definition.uri_segment
            )
            instance = self._ed_instance_registry.get(resource_type)
        return instance

    @property
    def collection_editor(self):
        return self._editor_instance('collection')  # type: CollectionEditor

    @property
    def manifest_editor(self):
        return self._editor_instance('manifest')  # type: ManifestEditor

    @property
    def sequence_editor(self):
        return self._editor_instance('sequence')  # type: SequenceEditor

    @property
    def canvas_editor(self):
        return self._editor_instance('canvas')  # type: CanvasEditor

    @property
    def layer_editor(self):
        return self._editor_instance('layer')  # type: LayerEditor
