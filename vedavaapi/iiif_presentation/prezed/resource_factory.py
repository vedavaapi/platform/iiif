from iiif_prezi.factory import *


def unset_default_uri_segment(cls, sub_classes_also=True):
    if hasattr(cls, '_uri_segment'):
        cls._uri_segment = ''
    if sub_classes_also:
        sub_classes = cls.__subclasses__()
        for subcls in sub_classes:
            unset_default_uri_segment(subcls)


Annotation._structure_properties = {'resource': {}}

# noinspection PyTypeChecker
unset_default_uri_segment(BaseMetadataObject)


class CustomManifestFactory(ManifestFactory):

    def __init__(self, image_api_client):
        super(CustomManifestFactory, self).__init__()
        self.image_api_client = image_api_client

    def image(self, ident, label="", iiif=False, region='full', size='full'):
        """Create an Image."""
        if not ident:
            raise RequirementError(
                "Images must have a real identity (Image['@id'] cannot be empty)")
        return CustomImage(self, ident, label, iiif, region, size)


class CustomImage(Image):
    def set_hw_from_iiif(self):
        """Set height and width from IIIF Image Information."""
        if not self._identifier:
            raise ConfigurationError(
                "Image is not configured with IIIF support")

        requrl = self._factory.default_base_image_uri + \
            "/" + self._identifier + '/info.json'
        try:
            data = self._factory.image_api_client.get(requrl)
        except:
            raise ConfigurationError(
                "Could not get IIIF Info from %s" % requrl)

        try:
            js = json.loads(data.text)
            self.height = int(js['height'])
            self.width = int(js['width'])
        except:
            print(data)
            raise ConfigurationError(
                "Response from IIIF server did not have mandatory height/width")
