import re
from functools import reduce

try:
    # noinspection PyCompatibility
    from urllib.parse import urljoin
except ImportError:  # Python 2
    # noinspection PyUnresolvedReferences,PyCompatibility
    from urlparse import urljoin

import flask_restx
from flask import Blueprint, request, g

from vedavaapi.common.helpers.api_helper import get_current_org

from .. import myservice, get_image_api_client, get_service_interface, get_image_api_conf, get_image_api_url_root, \
    get_fs_interface
from ...prezed.editor_factory import PresentationEditorFactory


api_blueprint_v1 = Blueprint(myservice().name + '_prezi', __name__)


api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title=myservice().title + ' Presentation API',
    description="Vedavaapi IIIF Presentation API",
    doc='/docs'
)


@api_blueprint_v1.route('')
def index():
    #  helper rout to get url_prefix dynamically
    return 'iiif image api v1'


def api_url_prefix():
    from flask import url_for, request
    index_url = url_for('.index')
    return re.match(request.environ['SCRIPT_NAME'] + r'(.*$)', index_url).groups()[0]


def get_editor_factory(service_name):
    image_api_conf = get_image_api_conf()
    image_api_client = get_image_api_client()

    base_image_uri = reduce(lambda base, segm: urljoin(base.rstrip('/') + '/', segm.lstrip('/')), [
        get_image_api_url_root(),
        get_current_org() and g.org_name,
        image_api_conf['prefix']
    ])

    editor_factory = PresentationEditorFactory.from_details(
        get_service_interface(service_name),
        get_fs_interface(service_name),
        image_api_client,
        request.url_root.rstrip('/') + '/' + api_url_prefix().lstrip('/'),
        None,  # TODO
        base_image_uri
    )  # type: PresentationEditorFactory
    return editor_factory


from . import rest
