import re
import sys

from flask import request, g
from vedavaapi.org_client import VedavaapiClient
from vedavaapi.common.helpers.api_helper import get_current_org

from .. import VedavaapiIiifPresentation


def myservice():
    return VedavaapiIiifPresentation.instance


def get_image_api_url_root():
    image_api_conf = myservice().image_api_conf()
    if 'url_root' in image_api_conf:
        return image_api_conf['url_root']
    return get_current_org() and g.original_url_root


def get_image_api_conf():
    return myservice().image_api_conf()


def get_image_api_client():
    return VedavaapiClient(get_image_api_url_root(), org_name=get_current_org() and g.org_name)


def get_service_interface(service_name):
    current_org_name = get_current_org()
    service_object = myservice().registry.lookup(service_name)
    if not hasattr(service_object, 'iiif_model_interface'):
        return None
    return service_object.iiif_model_interface(current_org_name)


def get_fs_interface(service_name):
    current_org_name = get_current_org()
    service_object = myservice().registry.lookup(service_name)
    if not hasattr(service_object, 'fs_interface'):
        return None
    return service_object.fs_interface(current_org_name)


from .v1 import api_blueprint_v1


blueprints_path_map = {
    api_blueprint_v1: '/v1'
}
