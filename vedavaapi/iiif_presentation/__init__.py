from vedavaapi.common import VedavaapiService, OrgHandler
from vedavaapi.org_client import VedavaapiClient


class IIIFPresentationOrgHandler(OrgHandler):

    def __init__(self, service, org_name):
        super(IIIFPresentationOrgHandler, self).__init__(service, org_name)
        self.image_api_client = VedavaapiClient(self.service.image_api_conf()['url_root'], org_name=self.org_name)


class VedavaapiIiifPresentation(VedavaapiService):

    instance = None  # type: VedavaapiIiifPresentation

    dependency_services = []
    org_handler_class = IIIFPresentationOrgHandler

    title = 'Vedavaapi IIIF presentation'
    description = 'Vedavaapi IIIF Presentation api'

    def __init__(self, registry, name, conf):
        super(VedavaapiIiifPresentation, self).__init__(registry, name, conf)

    def image_api_conf(self):
        return self.config['iiif_image_api']

    def image_api_client(self, org_name):
        return self.get_org(org_name).image_api_client
