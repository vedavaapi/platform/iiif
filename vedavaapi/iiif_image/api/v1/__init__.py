import os.path

import flask_restx
from flask import Blueprint

from .. import myservice


api_blueprint_v1 = Blueprint(myservice().name + '_image', __name__)


api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title=myservice().title + ' Image API',
    description="Vedavaapi IIIF Image API",
    doc='/docs'
)


@api_blueprint_v1.route('')
def index():
    #  helper rout to get url_prefix dynamically
    return 'iiif image api v1'


def api_url_prefix():
    from flask import url_for, request
    index_url = url_for('.index')
    import re
    return re.match(request.environ['SCRIPT_NAME'] + r'(.*$)', index_url).groups()[0]


from . import rest
