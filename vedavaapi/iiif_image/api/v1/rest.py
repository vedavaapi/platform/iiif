# import sys
import os

import flask_restx
import flask
from flask import request
from vedavaapi.common.helpers.api_helper import get_current_org
from ...loris.resolver import FSInterface

from .. import get_loris_wrapper, myservice
from . import api, api_url_prefix


def get_fs_interface(service_name):
    service_obj = myservice().registry.lookup(service_name)
    if not hasattr(service_obj, 'fs_interface'):
        return None
    return service_obj.fs_interface(get_current_org())


def get_external_iiif_service(service_name, ident):
    fs_interface = get_fs_interface(service_name)  # type: FSInterface
    if not fs_interface:
        return None
    oold = fs_interface.get_ool_data(ident)
    if not oold:
        return None
    namespace_data = oold.get('namespaceData', None)
    if namespace_data is None:
        return None
    iiif_image_resources = fs_interface.get_iiif_image_resource(oold)
    if not iiif_image_resources:
        return None
    image_resource = iiif_image_resources[0] if isinstance(iiif_image_resources, list) else iiif_image_resources
    try:
        service_id = image_resource['service']['@id']
    except KeyError:
        return None

    return service_id


# noinspection PyMethodMayBeStatic,PyUnusedLocal,PyShadowingBuiltins
@api.route('/<service_name>/<ident>/<region>/<size>/<rotation>/<quality>.<format>')
class Image(flask_restx.Resource):

    def get(self, service_name, ident, region, size, rotation, quality, format):
        external_iiif_service_id = get_external_iiif_service(service_name, ident)
        if external_iiif_service_id:
            external_iiif_image_url = os.path.join(external_iiif_service_id, '{}/{}/{}/{}.{}'.format(
                region, size, rotation, quality, format))
            return flask.redirect(external_iiif_image_url)

        loris_wrapper = get_loris_wrapper()
        return loris_wrapper.image_response(request, api_url_prefix())


# noinspection PyMethodMayBeStatic,PyUnusedLocal
@api.route('/<service_name>/<ident>/info.json', endpoint='info')
class Info(flask_restx.Resource):

    def get(self, service_name, ident):
        external_iiif_service_id = get_external_iiif_service(service_name, ident)
        if external_iiif_service_id:
            external_iiif_image_info_url = os.path.join(external_iiif_service_id, 'info.json')
            return flask.redirect(external_iiif_image_info_url)

        loris_wrapper = get_loris_wrapper()
        return loris_wrapper.info_response(request, api_url_prefix())


# noinspection PyMethodMayBeStatic
@api.route('/<service_name>/<ident>/')
class IdentRoot(flask_restx.Resource):

    def get(self, service_name, ident):
        return flask.redirect(api.url_for(Info, service_name=service_name, ident=ident))
