from vedavaapi.common.helpers.api_helper import get_current_org

from .. import VedavaapiIiifImage


def myservice():
    return VedavaapiIiifImage.instance


def get_loris_wrapper():
    current_org_name = get_current_org()
    return myservice().loris_wrapper(current_org_name)


from .v1 import api_blueprint_v1


blueprints_path_map = {
    api_blueprint_v1: "/v1"
}
