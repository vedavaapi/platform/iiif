import re
import os.path

try:
    # noinspection PyCompatibility
    from urllib.parse import unquote
except ImportError:  # Python 2
    # noinspection PyUnresolvedReferences
    from urllib import unquote


from loris.webapp import LorisRequest, BadRequestResponse

from . import IdentSwizzler


class CustomLorisRequest(LorisRequest):

    def __init__(self, request, api_url_prefix, redirect_id_slash_to_info=True, proxy_path=None):
        self.api_url_prefix = api_url_prefix
        super(CustomLorisRequest, self).__init__(
            request,
            redirect_id_slash_to_info=redirect_id_slash_to_info,
            proxy_path=proxy_path
        )
        self._correct_ident()

    def _correct_ident(self):
        service_name, ident = re.match(
            self.api_url_prefix.strip('/') + '/' + r'([^/]+)/([^/]+)', unquote(self.ident)
        ).groups()
        corrected_ident = IdentSwizzler.marshal(service_name, ident)
        self.ident = corrected_ident

    @property
    def base_uri(self):
        if self._proxy_path is not None:
            uri = '%s%s' % (self._proxy_path, self.ident)
        elif self._request.script_root != '':
            uri = os.path.join(self._request.url_root, self.api_url_prefix.lstrip('/'), self.ident)
        else:
            uri = os.path.join(self._request.host_url, self.api_url_prefix.lstrip('/'), self.ident)
        return uri


class LorisWrapper(object):

    def __init__(self, loris):
        self.loris = loris

    # noinspection PyProtectedMember
    def image_response(self, request, mount_uri):
        """
        IIIF Image retrieval
        should be called if we are sure request type is 'image'.
        otherwise it will return error response.
        :param request: request object
        :param mount_uri: uri segment at which api is mounted relative to host addr.
        :return: loris response
        """
        loris_request = CustomLorisRequest(
            request, mount_uri,
            redirect_id_slash_to_info=self.loris.redirect_id_slash_to_info,
            proxy_path=self.loris.proxy_path
        )
        if loris_request.request_type != 'image':
            return BadRequestResponse()

        ident = loris_request.ident
        base_uri = loris_request.base_uri
        params = loris_request.params
        fmt = params['format']
        if fmt not in self.loris.app_configs['transforms']['target_formats']:
            return BadRequestResponse('"%s" is not a supported format' % (fmt,))
        quality = params['quality']
        rotation = params['rotation']
        size = params['size']
        region = params['region']

        # noinspection PyProtectedMember
        response = self.loris.get_img(loris_request._request, ident, region, size, rotation, quality, fmt, base_uri)
        return self.set_acao(response, loris_request._request)

    # noinspection PyProtectedMember
    def info_response(self, request, mount_uri):
        """
        IIIF info.json
        :param request: request object
        :param mount_uri: mount_uri
        :return: info.json as json
        """
        loris_request = CustomLorisRequest(
            request, mount_uri,
            redirect_id_slash_to_info=self.loris.redirect_id_slash_to_info,
            proxy_path=self.loris.proxy_path
        )
        ident = loris_request.ident
        base_uri = loris_request.base_uri
        # print(ident, base_uri)
        response = self.loris.get_info(request, ident, base_uri)
        return self.set_acao(response, loris_request._request)

    # noinspection PyMethodMayBeStatic
    def set_acao(self, response, request):
        # print('iiiii', request.headers.get('Origin', request.url_root), file=sys.stderr)
        response.headers['Access-Control-Allow-Origin'] = request.headers.get('Origin', request.url_root)
        response.headers['Vary'] = 'Origin'
        return response
