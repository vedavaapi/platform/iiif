# import sys
import glob
import os
from logging import getLogger

import magic
from loris import constants
from loris.img_info import ImageInfo
# noinspection PyProtectedMember
from loris.resolver import _AbstractResolver, ResolverException, SimpleHTTPResolver
from vedavaapi.common.helpers.api_helper import get_current_org

from .. import VedavaapiIiifImage

from . import IdentSwizzler

logger = getLogger(__name__)


class FSInterface(object):

    def get_ool_data(self, oold_id):
        pass

    def get_namespace(self, oold):
        pass

    def provides_external_iiif(self, oold):
        pass

    def resolve_to_absolute_path(self, oold):
        pass

    def get_iiif_image_resource(self, oold):
        pass


class VedavaapiFSInterface(FSInterface):

    def __init__(self, org_name, colln, data_dir_path):
        self.org_name = org_name
        self.colln = colln
        self.data_dir_path = data_dir_path

    def get_ool_data(self, oold_id):
        return self.colln.find_one({"_id": oold_id}, projection={"permissions": 0})

    def get_namespace(self, oold):
        return oold.get('namespace', None)

    def provides_external_iiif(self, oold):
        if not 'namespaceData' in oold:
            return False
        if 'iiif_image' not in oold['namespaceData']:
            return False
        return True

    def resolve_to_absolute_path(self, oold):
        if oold.get('namespace', None) != '_vedavaapi':
            return None
        local_namespace_identifier = oold['identifier']
        absolute_path = os.path.join(self.data_dir_path, local_namespace_identifier)
        return absolute_path

    def get_iiif_image_resource(self, oold):
        namespace_data = oold.get('namespaceData', None)
        if not namespace_data:
            return None
        if 'iiif_image' not in namespace_data:
            return None
        return namespace_data['iiif_image']


class VedavaapiNamespaceResolver(_AbstractResolver):
    def is_resolvable(self, ident):
        return True  # Any way we have to read oold.

    @classmethod
    def get_mimetype(self, oold, source_fp):
        mimetype = oold.get('namespaceData', {}).get('mimetype', None)
        if not mimetype:
            magic_handler = magic.Magic(mime=True)
            mimetype = magic_handler.from_file(source_fp)
        return mimetype

    def resolve_path_format(self, ident, fs_interface, oold):
        source_fp = fs_interface.resolve_to_absolute_path(oold)
        if source_fp is None:
            VVResolver.raise_404_for_ident(ident)

        mimetype = self.get_mimetype(oold, source_fp)
        format_ = VVResolver.format_from_mimetype(mimetype)
        return source_fp, format_


class WebNamespaceResolver(SimpleHTTPResolver):
    def cache_file_extension(self, ident, response):
        if 'content-type' in response.headers:
            response.headers['content-type'] = response.headers['content-type'].split(';')[0]
        return super(WebNamespaceResolver, self).cache_file_extension(ident, response)

    def resolve_path_format(self, ident, fs_interface, oold):
        url = oold.get('url', None)
        if not url:
            VVResolver.raise_404_for_ident(ident)

        cached_file_path = self.cached_file_for_ident(url)
        if not cached_file_path:
            cached_file_path = self.copy_to_cache(url)

        format_ = self.get_format(cached_file_path, None)
        return cached_file_path, format_


class AbstractNamespaceResolver(_AbstractResolver):

    # noinspection PyUnusedLocal,PyMethodMayBeStatic
    def resolve_path_format(self, ident, fs_interface, oold):
        org_name = get_current_org()
        resolver = VedavaapiIiifImage.instance.registry.lookup(
            'abstractfs').resolver(org_name)
        af = resolver.get(oold)

        format_ = VVResolver.format_from_mimetype(af.mimetype)
        return af.cached_file_path, format_


class VVResolver(_AbstractResolver):
    def __init__(self, config):
        super(VVResolver, self).__init__(config)
        subconfig = self.config.get('sub', {})
        self.sub_resolvers = {
            "_vedavaapi": VedavaapiNamespaceResolver(subconfig.get('_vedavaapi', {})),
            "_web": WebNamespaceResolver(subconfig.get('_web', {})),
            "_abstract": AbstractNamespaceResolver(subconfig.get('_abstract', {}))
        }

    @classmethod
    def raise_404_for_ident(self, ident):
        message = 'Source image not found for identifier: %s.' % (ident,)
        logger.warning(message)
        raise ResolverException(message)

    def get_fs_interface(self, service_name, org_name):
        service_obj = VedavaapiIiifImage.instance.registry.lookup(service_name)
        if not hasattr(service_obj, 'fs_interface'):
            return None
        return service_obj.fs_interface(org_name)

    def get_oold(self, ident):
        org_name = get_current_org() # TODO org_name should be retrieved from somewhere else.
        ident_parts = IdentSwizzler.swizzle(ident)
        if ident_parts is None:
            return None
        service_name, _id = ident_parts.values()

        fs_interface = self.get_fs_interface(service_name, org_name)
        if not fs_interface:
            return None, None
        return fs_interface, fs_interface.get_ool_data(_id)

    @classmethod
    def format_from_mimetype(cls, mimetype):
        if not mimetype:
            raise ResolverException('unsupported mimetype')
        extension = mimetype.split('/')[-1]
        if len(extension) < 5:
            extension = extension.lower()
            return constants.EXTENSION_MAP.get(extension, extension)
        else:
            raise ResolverException('unsupported mimetype')

    def is_resolvable(self, ident):
        return True # Any way we have to load oold.

    def resolve(self, app, ident, base_uri):
        fs_interface, oold = self.get_oold(ident)
        if not oold:
            self.raise_404_for_ident(ident)
        namespace = oold.get('namespace', None)
        sub_resolver = self.sub_resolvers.get(namespace)
        if not sub_resolver:
            self.raise_404_for_ident(ident)

        source_fp, format_ = sub_resolver.resolve_path_format(ident, fs_interface, oold)
        auth_rules = self.get_auth_rules(ident, source_fp)
        return ImageInfo(
            app=app, src_img_fp=source_fp, src_format=format_, auth_rules=auth_rules)
