"""
additions, modifications to Loris to use with flask, and our framework
"""
try:
    # noinspection PyCompatibility
    from urllib.parse import unquote
except ImportError:  # Python 2
    # noinspection PyUnresolvedReferences
    from urllib import unquote

from collections import OrderedDict


class IdentSwizzler(object):

    @staticmethod
    def swizzle(ident):
        ident_split = unquote(ident).split('/', maxsplit=1)
        if len(ident_split) != 2:
            return None
        return OrderedDict([
            ('service_name', ident_split[0]),
            ('_id', ident_split[1])
        ])

    @staticmethod
    def marshal(*args, **kwargs):
        if len(args) >= 2:
            try:
                return '/'.join(args)
            except TypeError:
                return None
        elif len(kwargs) >= 2:
            try:
                return '/'.join((kwargs['service_name'], kwargs['_id']))
            except KeyError:
                return None
        else:
            return None
