import json

import six
import os.path

from vedavaapi.common import VedavaapiService, OrgHandler, unicode_for

from loris.webapp import read_config, Loris

from .loris.webapp import LorisWrapper


mydir = unicode_for(os.path.dirname(os.path.abspath(__file__)))


class IiifImageOrgHandler(OrgHandler):
    def __init__(self, service, org_name):
        super(IiifImageOrgHandler, self).__init__(service, org_name)

        self.loris_conf_template = self.service.loris_conf_template
        self.loris_conf = self.loris_conf_template.copy()
        loris_config_update = self._absolute_fp_val(self.service.loris_fp_conf_update)
        for key in self.loris_conf:
            if not isinstance(self.loris_conf[key], dict):
                continue
            self.loris_conf[key].update(loris_config_update.get(key, {}))

        self.loris = Loris(app_configs=self.loris_conf)
        self.loris_wrapper = LorisWrapper(self.loris)

    def _absolute_fp_val(self, valobj):
        if isinstance(valobj, six.string_types):
            components = valobj.split(':', maxsplit=1)
            if len(components) == 1:
                return valobj

            service_name = components[0] or self.service.name
            rel_path = components[1]
            file_store_type = rel_path.split('/', maxsplit=1)[0]
            base_path = rel_path.split('/', maxsplit=1)[1]
            # print(base_path, base_path.endswith('/'))
            return self.store.file_store_path(
                file_store_type=file_store_type,
                base_path=base_path,
                is_dir=base_path.endswith('/')
            )
        elif isinstance(valobj, list):
            return [self._absolute_fp_val(item) for item in valobj]
        elif isinstance(valobj, dict):
            return {key: self._absolute_fp_val(val) for (key, val) in valobj.items() if key != '_comment'}
        else:
            return valobj


class VedavaapiIiifImage(VedavaapiService):

    instance = None  # type: VedavaapiIiifImage

    dependency_services = []
    org_handler_class = IiifImageOrgHandler

    title = 'Vedavaapi IIIF Image'
    description = "Vedavaapi IIIF Image api"

    def __init__(self, registry, name, conf):
        super(VedavaapiIiifImage, self).__init__(registry, name, conf)
        self.loris_conf_template = self._loris_conf_template()
        self.loris_fp_conf_update = self._loris_fp_conf_update()

    # noinspection PyMethodMayBeStatic
    def _loris_conf_template(self):
        loris_conf_file_path = os.path.join(mydir, 'loris/loris2.conf')
        loris_conf = read_config(loris_conf_file_path)
        import getpass
        loris_conf['loris.Loris']['run_as_user'] = getpass.getuser()
        loris_conf['loris.Loris']['run_as_group'] = getpass.getuser()
        return loris_conf
        # return read_config(loris_conf_file_path)

    def _loris_fp_conf_update(self):
        p = os.path.join(mydir, 'loris/loris2_fp_conf_update.json')
        with open(p, 'rb') as pf:
            return json.loads(pf.read().decode('utf-8'))

    def loris(self, org_name):
        return self.get_org(org_name).loris

    def loris_wrapper(self, org_name):
        return self.get_org(org_name).loris_wrapper
